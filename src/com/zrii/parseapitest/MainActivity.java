package com.zrii.parseapitest;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;

import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseTwitterUtils;
import com.parse.ParseUser;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Parse.initialize(this, "Is6ByhAWZ7iIVTxJwupVxGXmZA7CNOICXpeeoLqr",
				"fND1nAicic1fLUaY7LGnWHoGWGs0bzoOu6ou0WNX");
		ParseAnalytics.trackAppOpened(getIntent());
		ParseTwitterUtils.initialize("zj036KJSsJbEZTN3IfA",
				"rszy7SJfXCpndnajxQ4s6SU47KM61OiEDmQAudcM");
		ParseTwitterUtils.logIn(this, new LogInCallback() {
			@Override
			public void done(ParseUser user, ParseException err) {
				if (user == null) {
					Log.d("MyApp",
							"Uh oh. The user cancelled the Twitter login.");
				} else if (user.isNew()) {
					Log.d("MyApp",
							"User signed up and logged in through Twitter!");
				} else {
					Log.d("MyApp", "User " + user.getUsername()
							+ " logged in through Twitter!");
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
